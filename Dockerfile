FROM node:19-alpine3.16

WORKDIR /usr/src/app

COPY ./app/package*.json ./
RUN npm install
COPY ./app .

EXPOSE 3001
CMD ["npm", "start"]
